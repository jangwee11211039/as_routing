/*************************************************************************
	> File Name: sparse_matrix.h
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月22日 星期二 15时48分12秒
	> Description: 基于链表结构实现的稀疏矩阵
 ************************************************************************/

#ifndef SPARSE_MATRIX_H_
#define SPARSE_MATRIX_H_

#include<stdio.h>
#include<stdlib.h>

#define MAXSIZE  70000 /*链表最大数量*/
#define INF 65535 /*不可达距离*/

typedef int ElemType;

typedef struct list_node{
	struct list_node* next;
	ElemType ele;
}list_node;

typedef struct{
	int num_usr;
	list_node* head[MAXSIZE];
}sparse_matrix;

//接口函数：


//函数功能：
//		初始化稀疏矩阵
//传入参数：
//		M:稀疏矩阵 num_usr:行数及列数的最大值
void inital_sparse_matrix(sparse_matrix* M,int num_usr);
//函数功能：
//		插入元素到稀疏矩阵
//传入参数：
//		M:稀疏矩阵 row:元素所在行 clo:元素所在列
void insert_elem(sparse_matrix* M,int row,int clo);
//函数功能：
//		插入元素到稀疏矩阵
//传入参数：
//		M:稀疏矩阵 row:元素所在行 clo:元素所在列
int find_elem(sparse_matrix* M,int row,int clo);
//函数功能：
//		根据稀疏矩阵，更新全局变量
//传入参数：
//		M:稀疏矩阵 index_row: 所要更新节点在稀疏矩阵的行位置 vec_distance:位置距离向量 vec_previous:前向节点向量
void update_vectors(sparse_matrix* M,int index_row,int* vec_distance,int* vec_previous);
//函数功能：
//		打印稀疏矩阵
//传入参数：
//		M:稀疏矩阵
void print_all(sparse_matrix* M);

#endif /*sparse_matrix.h*/