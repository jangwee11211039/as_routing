/*************************************************************************
	> File Name: sparse_matrix.c
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月22日 星期二 15时48分12秒
 ************************************************************************/


#include "sparse_matrix.h"

void inital_sparse_matrix(sparse_matrix* M,int num_usr)
{
	int i;
	M->num_usr=num_usr;
	for(i=1;i<=num_usr;++i)
	{
		M->head[i]=NULL;
	}
}

void insert_elem(sparse_matrix* M,int row,int clo)
{
	list_node* pnext;
	list_node* pnode=(list_node*)malloc(sizeof(list_node));
	pnode->next=NULL;
	pnode->ele=clo;	

	if (row==clo)
	{
		return;
	}

	if(!M->head[row])
	{
		M->head[row]=pnode;
		return ;
	}

	pnext=M->head[row];
	while(pnext->next)
	{
		pnext=pnext->next;
	}
	pnext->next=pnode; 
}


int find_elem(sparse_matrix *M,int row,int clo)
{
	ElemType ele;
	list_node* pnode=M->head[row];
	while(pnode)
	{
		ele=pnode->ele;
		if(ele==clo)
		{
			return 1;
		}
		pnode=pnode->next;
	}
	return 0;
}

void print_all(sparse_matrix* M)
{
	int i;
	list_node* pnode;
	for(i=1;i<=M->num_usr;++i)
	{
		printf("\n node %d",i);
		pnode=M->head[i];
		while(pnode)
		{
			printf("\t %d",pnode->ele);
			pnode=pnode->next;
		}
	}
}

void update_vectors(sparse_matrix* M,int index_row,int* vec_distance,int* vec_previous)
{
	list_node* pnode;
	ElemType ele;
	if(index_row>M->num_usr)
	{
		printf("\n error: update_vector: index_row out of bound!");
		return ;
	}
	pnode=M->head[index_row];
	/*利用该数据的特点（权重为一），所以仅仅对当前不可达的节点进行更新即可*/
	while(pnode)
	{
		ele=pnode->ele;
		if(*(vec_distance+ele)==INF)
		{	
			*(vec_distance+ele)=*(vec_distance+index_row)+1;
			*(vec_previous+ele)=index_row;
		}
		pnode=pnode->next;
	}
}

