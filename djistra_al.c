/*************************************************************************
	> File Name: djistra.c
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月22日 星期二 21时18分03秒
 ************************************************************************/
#include "djistra_al.h"


int distance_of_index_point[MAXSIZE+1];
int set_added_point_of_index_point[MAXSIZE+1];
int previous_point_of_index_point[MAXSIZE+1];
int flag[MAXSIZE+1];
int actual_usr;


void djistra(sparse_matrix* M,int num_usr)
{
	int i,j,k;/*遍历常量*/
	int last_point;/*记录上一次添加节点，便于早点跳出*/
	actual_usr=0;/*统计实际用户数量，全局变量*/
	
	/*初始化变量*/
	for(i=1;i<=num_usr;++i)
	{
		distance_of_index_point[i]=INF;
		set_added_point_of_index_point[i]=0;
		flag[i]=1;
	}
	last_point=-1;
	distance_of_index_point[1]=0;

	/*更新节点1到其他节点的属性*/
	update_vectors(M,1,distance_of_index_point,previous_point_of_index_point);

	/*逐步添加枝干*/
	for(i=1;i<=num_usr;++i)
	{
		int min_dis=INF;
		
		/*寻找当前最近的路由器节点*/
		for(j=1;j<=num_usr;++j)
		{
			if((flag[j])&&(min_dis>distance_of_index_point[j]))
			{
				min_dis=distance_of_index_point[j];
				k=j;/*记录当前最近节点*/
			}
		}
		/*判断是否已经把所有节点都添加，是则跳出完成构建树*/
		if(last_point==k)
		{
			break;
		}
		/*将节点添加到树*/
		set_added_point_of_index_point[i]=k;
		/*记录上一次加入的节点*/
		last_point=k;
		/*置标志位，此后不再遍历*/
		flag[k]=0;
		/*统计实际Router数量*/
		++actual_usr;
		/*通过利用新加入的节点信息，更新节点1到其他Router的属性*/
		update_vectors(M,k,distance_of_index_point,previous_point_of_index_point);
	}
}

/*递归回溯数据，不断得到前向节点*/
void get_previous(int now_point)
{
	int i;
	if(now_point==1)
	{
		printf("\n[1");
		return;
	}
	else
	{
		i=previous_point_of_index_point[now_point];
		get_previous(i);
		printf("-->%d",now_point);
	}
}

void out_put_topology(int num_usr,const char* file_name)
{
	int i,usr_id,len;
	int point;
	len=1;
	for(i=2;i<=num_usr;++i)
	{
		if(usr_id!=set_added_point_of_index_point[i])
		{
			usr_id=set_added_point_of_index_point[i];
			++len;
		}
	}
	/*文件重定向*/
	freopen(file_name,"w",stdout);
	/**/
	printf("\n#Curriculum\tBasic Information Net\tTeacher\tYuchun Guo");
	printf("\n#Student\tID\t11211039\tName\tJangWee\tClass\tTX1102");
	printf("\n#Format [ROUTE]\t[TTL]");
	/*按距离由近及远打印*/
	for(i=1;i<=len;++i)
	{
		point=set_added_point_of_index_point[i];
		get_previous(point);
		printf("]\t[%d]",distance_of_index_point[point]);
	}
}

/*打印全局变量，调试用*/
void print_all_arrays(int usr_num,const char* file_name)
{

	int i;
	freopen(file_name,"w",stdout);
	if(usr_num>MAXSIZE)
	{
		printf("\n out of bound \n");
		return ;
	}
	printf("\n set_added :");
	for(i=1;i<=usr_num;++i)
	{
		printf("%d \t",set_added_point_of_index_point[i]);
	}
	printf("\n");

	printf("\n distance :");
	for(i=1;i<=usr_num;++i)
	{
	printf("%d \t",distance_of_index_point[i]);
	}
	printf("\n");

	printf("\n previous_point :");
	for(i=1;i<=usr_num;++i)
	{
		printf("%d \t",previous_point_of_index_point[i]);
	}
	printf("\n");

	printf("\n flag :");
	for(i=1;i<=usr_num;++i)
	{
		printf("%d \t",flag[i]);
	}
	printf("\n");
}

