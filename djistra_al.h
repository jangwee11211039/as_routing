/*************************************************************************
	> File Name: djistra.h
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月22日 星期二 21时18分03秒
	> Description: 实现Djistra算法。
 ************************************************************************/

#ifndef DJISTRA_AL_H_
#define DJISTRA_AL_H_

#include "sparse_matrix.h"
#include<stdio.h>
#include <stdlib.h>

//全局变量
//index 节点索引 [0] not used
extern int distance_of_index_point[MAXSIZE+1];/*节点1到其他路由节点的距离*/
extern int previous_point_of_index_point[MAXSIZE+1];/*各节点路由器的前向节点*/
extern int flag[MAXSIZE+1];/*各节点标志位*/
extern int set_added_point_of_index_point[MAXSIZE+1];/*已处理的节点路由器*/
extern int actual_usr;/*实际用户数*/

//接口函数

// 函数功能：
//		Djistra算法的具体实现
// 传入参数：		
//		M：链表结构实现的稀疏矩阵  num_usr: 局域系统内的Router数量
void djistra(sparse_matrix* M,int num_usr);

//函数功能：	
//		输出拓扑结构到文件
//传入参数：
//		num_usr:局域系统内的Router数量 file_name: 重定向文件名
void out_put_topology(int num_usr,const char* file_name);

//函数功能：
//		获取前向Router节点
//传入参数：
//		now_point: 当前Router节点索引
void get_previous(int now_point_index);

//函数功能：
//		打印全局变量，便于调试
//传入参数：
//		num_usr:Router节点数量 file_name:重定向文件名
void print_all_arrays(int num_usr,const char* file_name);

#endif	/*djistra_al.h*/
