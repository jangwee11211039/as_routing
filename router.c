#include "djistra_al.h"
#include "sparse_matrix.h"
#include <stdio.h>
#include <stdlib.h>
//#include <direct.h>
#include<unistd.h>

#define LINE 1024/*数组大小*/

int main()
{
	
	FILE* fp;/*数据文件指针*/
	char* line_buffer;/*行读取缓冲区指针*/
	sparse_matrix* matrix;/*稀疏矩阵指针*/
	int i;/*遍历计数*/
	int num_router=0;/*路由器编号*/

	/*获取当前目录，组件输入输出文件名，文件重定向*/
	char current_path[LINE];
	char source_data_file[LINE];
	char output_data_file[LINE];
	char* data_file_name="/ASconnpairs19971108.txt";
	char* output_file_name="/ASRoutingTable.txt";
	getcwd(current_path,LINE);
	strcpy(source_data_file,current_path);
	strcpy(output_data_file,current_path);
	strcat(source_data_file,data_file_name);
	strcat(output_data_file,output_file_name);

	/*文件读取失败*/
	if((fp=fopen(source_data_file,"r"))==NULL)
	{
		printf("\n Error:file not exits ");
		return -1;
	}

	printf("\nReading data from %s......",source_data_file);


	line_buffer=(char*)malloc(sizeof(char)*LINE);

	/*获取最大路由器编号*/
	while(fgets(line_buffer,LINE,fp))
	{
		int i_1,i_2;
		sscanf(line_buffer,"%d %d",&i_1,&i_2);
		num_router=(num_router>i_1?num_router:i_1);
		num_router=(num_router>i_2?num_router:i_2);
	}
	fclose(fp);
	
	printf("\nThe max ID of router :%d......",num_router);

	/*为稀疏矩阵分配内存并初始化*/
	matrix=(sparse_matrix*)malloc(sizeof(sparse_matrix));
	inital_sparse_matrix(matrix,num_router);

	if((fp=fopen(source_data_file,"r"))==NULL)
	{
		printf(" \n Error: file open error");
		return -1;
	}
	/*读取文件数据，构建稀疏矩阵*/
	while(fgets(line_buffer,LINE,fp))
	{
		int row,clo;
		sscanf(line_buffer,"%d %d",&row,&clo);
		insert_elem(matrix,row,clo);
		insert_elem(matrix,clo,row);
	}
	fclose(fp);

	/*插入对角元素*/
	for(i=1;i<=matrix->num_usr;++i)
	{
		insert_elem(matrix,i,i);
	}

	printf("\nParse data finish and calculating......");

	/*Djistra最短路径*/
	djistra(matrix,num_router);

	printf("\nWriting the topology data to file at :%s......",output_data_file);

	/*打印拓扑结构，输出结果*/
	out_put_topology(actual_usr,output_data_file);
	
	printf("\n#All finishing!");

	/*释放内存*/
	free(line_buffer);
	free(matrix);
	return 0;

}
